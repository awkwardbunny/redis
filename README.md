# build-your-own-redis

```bash
# To run server
go run ./cmd/redis-server

# To run test client
go run ./cmd/redis-client

# To test sending other bytes
nc localhost 6379
```

## references
[Concurrent TCP client/server](http://www.inanzzz.com/index.php/post/j3n1/creating-a-concurrent-tcp-client-and-server-example-with-golang)

[Redis Protocol Specification](https://redis.io/topics/protocol)

[Build Your Own Redis Blog Posts](https://rohitpaulk.com/articles/redis-0)

[TCP Sockets](https://ipfs.io/ipfs/QmfYeDhGH9bZzihBUDEQbCbTc5k5FZKURMUoUvfmc27BwL/socket/tcp_sockets.html)
