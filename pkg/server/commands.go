package server

func Do_COMMAND(args []interface{}) (interface{}, error) {
	resp := []interface{}{
		[]interface{}{ "ping", -1, []interface{}{"stale", "fast"}, 0, 0, 0, []interface{}{"@fast", "@connection"}},
		[]interface{}{ "info", -1, []interface{}{"random", "loading", "stale"}, 0, 0, 0, []interface{}{"@slow", "@dangerous"}},
	}

	return resp, nil
}

func Do_PING(args []interface{}) (interface{}, error) {
	if len(args) == 1 {
		return "PONG", nil
	}

	return args[1], nil
}