package common

import (
	"bufio"
	"errors"
	"fmt"
	"log"
)

func Serialize(data interface{}, depth int) ([]byte, error) {

	//for i := 0; i < depth; i++ {
	//	fmt.Print("-")
	//}
	//fmt.Printf("%v\n", data)

	if data == nil {
		return []byte("*-1\r\n"), nil
	}

	switch data.(type) {
	case string:
		return []byte(fmt.Sprintf("+%s\r\n", data)), nil
	case int:
		return []byte(fmt.Sprintf(":%d\r\n", data)), nil
	case []interface{}:
		arr := data.([]interface{})

		retval := []byte(fmt.Sprintf("*%d\r\n", len(arr)))
		for _, e := range arr {
			v, err := Serialize(e, depth+1)
			if err != nil {
				return nil, err
			}
			retval = append(retval, v...)
		}
		return retval, nil
	default:
		return nil, nil
	}
}

func Deserialize(r *bufio.Reader) (interface{}, error) {

	// Read
	data, err := r.ReadString('\n')
	if err != nil {
		return nil, err
	}

	switch data[0] {
	case byte('+'):
		val := ""
		n, err := fmt.Sscanf(data, "+%s\r\n", &val)
		if err != nil || n < 1 {
			log.Println(err)
			return val, errors.New("Cant parse simple string")
		}
		return val, nil

	case byte(':'):
		val := 0
		n, err := fmt.Sscanf(data, ":%d\r\n", &val)
		if err != nil || n < 1 {
			log.Println(err)
			return val, errors.New("Cant parse int")
		}
		return val, nil

	case byte('$'):
		strLen := 0
		n, err := fmt.Sscanf(data, "$%d\r\n", &strLen)
		if err != nil || n < 1 {
			log.Println(err)
			return nil, errors.New("Cant parse bulk string length")
		}

		if strLen == -1 {
			return nil, nil
		}

		data, err = r.ReadString('\n')
		if err != nil {
			return nil, err
		}

		val := ""
		n, err = fmt.Sscanf(data, "%s\r\n", &val)
		if err != nil || n < 1 {
			log.Println(err)
			return val, errors.New("Cant parse bulk string")
		}

		//fmt.Printf("len: %d  actual: %d\n", strLen, len(val))
		return val, nil

	case byte('*'):
		arrLen := 0
		n, err := fmt.Sscanf(data, "*%d\r\n", &arrLen)
		if err != nil || n < 1 {
			log.Println(err)
			return nil, errors.New("Cant parse array length")
		}

		arr := make([]interface{}, arrLen)
		for i := 0; i < arrLen; i++ {
			arr[i], err = Deserialize(r)
			if err != nil {
				return nil, err
			}
		}

		return arr, nil

	// TODO: error type
	case byte('-'):
		return nil, errors.New("Error type not yet implemented")

	default:
		return nil, errors.New("Unknown type")
	}
}