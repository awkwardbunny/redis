package main

import (
	"bufio"
	"fmt"
	"io"
	"github.com/akamensky/argparse"
	"gitlab.com/awkwardbunny/redis"
	"log"
	"net"
	"os"
)

func main() {

	// ArgParse
	parser := argparse.NewParser("redis-client", "Brian's redis client")
	addr := parser.String("a", "address", &argparse.Options{Help: "Server address to connect to", Default: redis.DEFAULT_HOST})
	port := parser.Int("p", "port", &argparse.Options{Required: false, Help: "Server port to connect to", Default: redis.DEFAULT_PORT})
	udp := parser.Flag("u", "udp", &argparse.Options{Help: "Use UDP instead of TCP", Default: false})

	// Parse args
	err := parser.Parse(os.Args)
	if err != nil {
		log.Fatalln(parser.Usage(err))
	}

	// UDP not supported
	if *udp {
		log.Fatalln("UDP not implemented yet!")
	}

	addr_port := fmt.Sprintf("%s:%d", *addr, *port)
	log.Printf("Connecting to server at %s\n", addr_port)

	// Connect to server
	tcpAddr, err := net.ResolveTCPAddr("tcp", addr_port)
	if err != nil {
		log.Fatalln("Error: " + err.Error())
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		log.Fatalln("Error: " + err.Error())
	}
	defer conn.Close()

	// Send loop
	running := true
	for running {
		fmt.Println("###################")
		fmt.Println("# 0: Send PING    #")
		fmt.Println("# 1: Exit         #")
		fmt.Println("# 2: Custom       #")
		fmt.Println("###################")

		var choice int
		_, err := fmt.Scanf("%d", &choice)
		if err != nil {
			log.Println("Error: " + err.Error())
		}

		reader := bufio.NewReader(conn)

		switch choice {
		case 0:
			//n, err := conn.Write([]byte("+PING\r\n"))
			n, err := conn.Write([]byte("*1\r\n$7\r\nCOMMAND\r\n"))
			if err != nil {
				log.Println("Error: " + err.Error())
			} else {
				log.Printf("Sent PING: (%d) %v\n", n, []byte("+PING\r\n"))

				for {
					data, err := reader.ReadString('\n')
					if err != nil {
						if err == io.EOF {
							break
						}
						log.Printf("Error: " + err.Error())
					}

					log.Printf("Received: (%d) %v\n", len(data), []byte(data))
					log.Printf("Data: %s\n", data)
				}
			}
		case 2:
			fmt.Print("> ")
			in, _ := bufio.NewReader(os.Stdin).ReadString('\n')
			in = in[:len(in)-1]
			in = in + "\r\n"

			n, err := conn.Write([]byte(in))
			if err != nil {
				log.Println("Error: " + err.Error())
			} else {
				log.Printf("Sent Data: (%d) %v\n", n, []byte(in))

				data, err := reader.ReadString('\n')
				if err != nil {
					log.Printf("Error: " + err.Error())
				}

				log.Printf("Received: (%d) %v\n", len(data), []byte(data))
			}
		case 1:
			running = false
		default:
			fmt.Println("Invalid")
		}
	}
	log.Println("Exiting...")
}
