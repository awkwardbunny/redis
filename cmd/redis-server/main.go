package main

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/akamensky/argparse"
	"gitlab.com/awkwardbunny/redis"
	"gitlab.com/awkwardbunny/redis/pkg/common"
	"gitlab.com/awkwardbunny/redis/pkg/server"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

func handleConnection(c net.Conn) {
	defer c.Close()
	log.Printf("Connection from %s\n", c.RemoteAddr().String())

	for {
		reader := bufio.NewReader(c)

		cmd, err := common.Deserialize(reader)
		if err != nil {
			if err != io.EOF {
				log.Println("Error: " + err.Error())
				return
			}

			log.Printf("Connection terminated from %s\n", c.RemoteAddr().String())
			return
		}

		log.Printf("CMD: %v\n", cmd)

		/** This below should somehow be in the server package? **/
		cmdList := cmd.([]interface{})
		var resp interface{}
		switch strings.ToLower(cmdList[0].(string)) {
		case "command":
			resp, _ = server.Do_COMMAND(cmdList)

		case "ping":
			resp, _ = server.Do_PING(cmdList)

		default:
			resp = errors.New("Unknown command")
		}
		/** Up to here **/

		log.Printf("RESP: %v\n", resp)
		respBytes, _ := common.Serialize(resp, 1)

		_, err = c.Write(respBytes)
		if err != nil {
			log.Println("Error: " + err.Error())
		//} else {
		//	log.Printf("Sent: (%d) %v\n", n, respBytes)
		}
	}
}

func main() {

	// ArgParse
	parser := argparse.NewParser("redis-server", "Brian's redis server")
	addr := parser.String("a", "address", &argparse.Options{Help: "Address to listen on", Default: redis.DEFAULT_HOST})
	port := parser.Int("p", "port", &argparse.Options{Required: false, Help: "Port to listen on", Default: redis.DEFAULT_PORT})
	udp := parser.Flag("u", "udp", &argparse.Options{Help: "Use UDP instead of TCP", Default: false})

	// Parse args
	err := parser.Parse(os.Args)
	if err != nil {
		log.Fatalln(parser.Usage(err))
	}

	// UDP not supported
	if *udp {
		log.Fatalln("UDP not implemented yet!")
	}

	addr_port := fmt.Sprintf("%s:%d", *addr, *port)
	log.Printf("Starting server on %s\n", addr_port)

	// Start listening
	tcpAddr, err := net.ResolveTCPAddr("tcp", addr_port)
	if err != nil {
		log.Fatalln("Error: " + err.Error())
	}

	listener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		log.Fatalln("Error: " + err.Error())
	}

	// Accept connection loop
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println("Error: " + err.Error())
		}

		go handleConnection(conn)
	}
}
